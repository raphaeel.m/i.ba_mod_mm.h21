package ch.hslu.mod.testat2;

import java.util.Date;

public class Produkt {
	
	private Produktbeschreibung produktbeschreibung;
	private Date verfallsdatum;

	public Produkt(Produktbeschreibung produktbeschreibung, Date verfallsdatum) {
		this.produktbeschreibung = produktbeschreibung;
		this.verfallsdatum = verfallsdatum;
	}
	
	public Produktbeschreibung getProduktbeschreibung() {
		return produktbeschreibung;
	}

	public void setProduktbeschreibung(Produktbeschreibung produktbeschreibung) {
		this.produktbeschreibung = produktbeschreibung;
	}

	public Date getVerfallsdatum() {
		return verfallsdatum;
	}

	public void setVerfallsdatum(Date verfallsdatum) {
		this.verfallsdatum = verfallsdatum;
	}
}
