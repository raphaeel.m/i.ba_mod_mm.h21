package ch.hslu.mod.testat2;

import java.util.Date;

public class VerkauftesProdukt {

	private String name;
	private float preis;
	private Date verkaufsdatum;
	
	public VerkauftesProdukt(String name, float preis, Date verkaufsdatum) {
		this.name = name;
		this.preis = preis;
		this.verkaufsdatum = verkaufsdatum;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPreis() {
		return preis;
	}
	public void setPreis(float preis) {
		this.preis = preis;
	}
	public Date getVerkaufsdatum() {
		return verkaufsdatum;
	}
	public void setVerkaufsdatum(Date verkaufsdatum) {
		this.verkaufsdatum = verkaufsdatum;
	}
}
