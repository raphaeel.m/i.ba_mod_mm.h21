package ch.hslu.mod.testat2;

public class Ebene {

	private int ebenenummer;
	private Spirale[] spiralen = new Spirale[6];

	public Ebene(int ebene) {
		this.ebenenummer = ebenenummer;
	}
	
	public void kaufen(int spiralnummer) {
		// Logik hier
	}
	
	public int getEbeneNummer() {
		return ebenenummer;
	}

	public void setEbeneNummer(int ebene) {
		this.ebenenummer = ebene;
	}
	
	public Spirale[] getSpiralen() {
		return spiralen;
	}

	public void setSpiralen(Spirale[] spiralen) {
		this.spiralen = spiralen;
	}
}
