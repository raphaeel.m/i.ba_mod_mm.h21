package ch.hslu.mod.testat2;

import java.util.ArrayList;

public class Produktautomat {
	
	private int id;
	private boolean servicemode;
	private Ebene[] ebenen = new Ebene[7];
	private ArrayList<VerkauftesProdukt> verkaufteProdukte = new ArrayList<VerkauftesProdukt>();
	private ArrayList<Produktbeschreibung> produktBeschreibungen = new ArrayList<Produktbeschreibung>();

	public Produktautomat() {
		for(int i = 0; i < ebene.length; i++) {
			ebene[i] = new Ebene()
		}
	}

	public void kaufen(int nummer) {
		// Logik hier
	}
	
	public void neuerVerkauf(Produkt produkt) {
		// Logik hier
	}
	
	public int getId() {
		return id;
	}
	
	public boolean getServicemode() {
		return servicemode;
	}
	
	public void setServicemode(boolean servicemode) {
		this.servicemode = servicemode;
	}
	
	public Ebene[] getEbenen() {
		return ebenen;
	}

	public void setEbenen(Ebene[] ebenen) {
		this.ebenen = ebenen;
	}
}
