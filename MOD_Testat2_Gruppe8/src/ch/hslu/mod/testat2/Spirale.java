package ch.hslu.mod.testat2;

import java.util.Date;

public class Spirale {

	private Produkt[] produkte = new Produkt[7];

	public Spirale() {
		for(int i = 0; i < produkte.length; i++) {
			produkte[i] = new Produkt("Mars", 2.0f)
		}
	}

	public void kaufen() {
		// Logik hier
	}
	
	public Produkt gibProdukt() {
		// Logik hier
		return new Produkt(new Produktbeschreibung("Mars", 2.0f), new Date());
	}
	
	public Produkt entferneProdukt() {
		// Logik hier
		return new Produkt(new Produktbeschreibung("Mars", 2.0f), new Date());
	}
	
	public void drehen() {
		// Logik hier
	}
	
	public Produkt[] getProdukte() {
		return produkte;
	}

	public void setProdukte(Produkt[] produkte) {
		this.produkte = produkte;
	}
}
